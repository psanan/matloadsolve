all : MatLoadSolve

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

MatLoadSolve: MatLoadSolve.o  chkopts
	-${CLINKER} -o MatLoadSolve MatLoadSolve.o  ${PETSC_KSP_LIB}
	${RM} MatLoadSolve.o

clean ::
	${RM} MatLoadSolve
