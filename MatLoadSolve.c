#include <petsc.h>

static char help[] = "Load a matrix, and solve with it.\n\n";

int main(int argc, char ** argv)
{
  Mat                A;
  Vec                x,b;
  KSP                ksp;
  PetscErrorCode     ierr;
  char               filename[PETSC_MAX_PATH_LEN];
  PetscBool          set,badfilename=PETSC_FALSE;
  const PetscScalar  one=1.0;
  size_t             strlen;

  PetscInitialize(&argc,&argv,(char*)0,help);

  ierr = PetscOptionsGetString(NULL,NULL,"-file",filename,PETSC_MAX_PATH_LEN,&set);CHKERRQ(ierr); 
  if (!set) {
    badfilename = PETSC_TRUE;
  }else{
    ierr = PetscStrlen(filename,&strlen);CHKERRQ(ierr);
    if (!strlen){
      badfilename = PETSC_TRUE;
    }
  }
  if (badfilename) {
      ierr = PetscPrintf(PETSC_COMM_WORLD,"You must provide a PETSc binary matrix file with -file <matfile>\n");CHKERRQ(ierr);
      return 1;
  }

  /* Load matrix */
  {
    PetscViewer matview;
    ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,filename,FILE_MODE_READ,&matview);CHKERRQ(ierr);
    ierr = MatCreate(PETSC_COMM_WORLD,&A);CHKERRQ(ierr);
    ierr = MatLoad(A,matview);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&matview);CHKERRQ(ierr);
  }

  /* Create a RHS of all ones (not always realistic!)  */
  ierr = MatCreateVecs(A,&b,NULL);CHKERRQ(ierr);
  ierr = MatCreateVecs(A,&x,NULL);CHKERRQ(ierr);
  ierr = VecSet(b,one);CHKERRQ(ierr);CHKERRQ(ierr);  /* TODO: add random option */

  /* Create Linear Solver */
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);

  /* Solve */
  ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);

  /* Clean Up */
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&b);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);

  return 0;
}
